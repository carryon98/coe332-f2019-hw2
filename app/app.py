from flask import Flask, jsonify, request, abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)

@app.route('/meeting')
def meeting():
    output = data["meeting"]
    return jsonify(output)

@app.route('/meeting/<string:field>')
def meeting_sub(field):
    output = data["meeting"][field]
    return jsonify(output)

@app.route('/instructors')
def instruct():
    output = data["instructors"]
    return jsonify(output)

# there's probs a better way to do this
@app.route('/instructors/<int:num>')
def instruct_sub0(num):
    if num == 1 or num == 2 or num == 0:
        output = data["instructors"][num]
        return jsonify(output)
    return abort(404)

@app.route('/assignments', methods= ['GET','POST'])
def assign():
    if request.method == 'POST':
        entered = request.data.decode('utf-8')
        json_acceptable_string = entered.replace("'", "\"")
        dic = json.loads(json_acceptable_string)
        data["assignments"].append(dic)
        return entered

    output = data["assignments"]
    return jsonify(output)

@app.route('/assignments/<int:number>')
def assign_sub(number):
    output = data["assignments"][number]
    return jsonify(output)

@app.route('/assignments/<int:number>/<string:url>')
def assign_sub_url(number,url):
    output = data["assignments"][number][url]
    return jsonify(output)